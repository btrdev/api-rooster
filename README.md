BTR Api Rooster instructies
=======================

Allereerst, zie BTR Api Basis instructies.

# Inhoudsopgave
- Initialisatie
    - Rooster URL
    - Locaties
    - Indexeren
- Werken met de index
- Roosters laden
    - De Api-call
    - Werken met het resultaat
- Structuurdiagrammen
    - todo

## Initialisatie

### Rooster URL
Als allereerste moet de Api Rooster (hierna Api) worden ingesteld.
Dit gaat simpelweg door bijvoorbeeld:

```java
ApiRooster.setRoosterUrl("http://rooster.calvijncollege.nl/");
```

Hiermee weet de Api dat hij deze url moet gebruiken om alle volgende requests op
te baseren.

### Locaties
De Api ondersteunt het gebruik van meerdere 'locaties'. Deze moeten op de
volgende manier worden toegevoegd:

```java
ApiRooster.LOCATIES.put("Goes", "Goes Klein Frankrijk");
...
ApiRooster.LOCATIES.put("Tholen", "Tholen");
```

Er kunnen zo veel locaties worden toegevoegd als nodig is.

### Indexeren
Wanneer alle locaties zijn toegevoegd, moet er een eenmalige indexering worden
gemaakt. Deze indexeeractie duurt zo'n 2 seconden.

```java
boolean gelukt = ApiRooster.indexeer();
```

Als de Api in de achtergrond draait, is het een goed idee om bijvoorbeeld ieder
uur de indexeerfunctie een keer aan te roepen om de index up-to-date te houden.

## Werken met de index
Als er succesvol een indexeeractie is uitgevoerd, is het resultaat beschikbaar
in `ApiRooster.INDEXERINGEN`. Dit is een `HashMap<String, Indexering>` waar
de key de slug van de locatie is.
Voor elke locatie is er dus een aparte `Indexering` gemaakt.

Zie [het structuurdiagram](#Indexering) voor de beschikbare calls.

## Roosters laden

### De Api-call

De gegevens die nodig zijn voor het laden van een rooster zijn: 
- __code__: *string*, leerlingnummer, docent-, klas- of ruimtecode
- __type__: *string*, s=leerling, t=docent, c=klas, r=ruimte
- __week__: *int*, weeknummer, te verkrijgen uit de index
- __locatie__: *string*, slug van de locatie (gegeven bij het instellen van locaties)

Hierna kan de Api worden aangeroepen:
```java
//LaadRooster.laadRooster(code, type, week, locatie);
LaadRooster.laadRooster("12345", "s", 37, "Goes");
```

Zoals elk Api-resultaat heeft de resultaatcontainer een functie om na te gaan
of de actie geslaagd is. Check dit met `.gelukt();`. Zie de kop 'Werken met
het resultaat' voor gedetailleerdere informatie over het roosterobject.

### Werken met het resultaat (LaadRoosterResultaat)

Het belangrijkste onderdeel van het `LaadRoosterResultaat` is het rooster op
zichzelf. Dat is te vinden onder `.getRooster2d();`. Dit geeft een
tweedimensionale array terug bestaande uit `BasisLes` (zie diagram).
Elk object in de eerste dimensie staat voor een dag. De index in deze dimensie
staat voor het dagnummer. De telling is als volgt: 0=zondag, 6=zaterdag.
Hierop volgen de uren zelf, de volgorde in de array zegt in principe niets!


## Structuurdiagrammen
(Vrijwel) alle velden uit de onderstaande structuurdiagrammen zijn via een getter
op te vragen. Zie de Javadoc voor een meer gedetailleerde beschrijving.

### Indexering
    weken List<Integer>: opsomming van beschikbare weken
    klassen Map<String, Integer>: mapping van klascode naar index
    docenten Map<String, Integer>: mapping van docentcode naar index
    lokalen Map<String, Integer>: mapping van lokaal-/ruimtecode naar index
    leerlingen Map<String, Integer>: mapping van leerlingnummer naar index
    laatstBijgewerktOp Date: bijwerkdatum van de data zelf

### BasisLes
    lesType String: korte identificator, zoals BasisLes, ZUurLes, etc.
    lesDoel LesDoel: bestemming van de les, S, T, R of C
    dag Integer: nummer van dag in de week (0=zo, 6=za)
    lesUur Integer: nummer van uur op de dag
    lesSpan Integer: hoe veel lesuren dit uur zich uitstrekt
    roosterTekst List<RoosterTekst>: opsomming van tekst
    getRoostertekstVolledig() String: kaal alle tekst
    isLeeg() Boolean: check of het een lege les is
    isRanduur() Boolean: check of het een randuur is (niets voor/na)
    heeftWijziging() Boolean: check of tekst rood is (wijziging)
    heeftDoorgestreept() Boolean: check of tekst doorgestreept is
    getVanTot(LaadRoosterResultaat) Pair<Date, Date>: 