package nl.jochembroekhoff.btr.api.rooster.container;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import nl.jochembroekhoff.btr.api.roostering.Naam;

/**
 * Leerling container met parseerder.
 *
 * @author Jochem Broekhoff
 */
@RequiredArgsConstructor
public class Leerling {

    private static int teller = 0;

    @Getter
    private final Naam naam;
    @Getter
    private final String leerlingnummer;

    /**
     * Verkrijg het leerlingnummer uit een naamtekst. Als het leerlingnummer
     * niet correct kan worden uitgelezen, wordt er een leerlingnummer
     * teruggegeven op basis van een teller. Dit is dan een letter gevolgd door
     * een tellende waarde. Voor een leerling is de voorgevoegde letter een s
     * van student.
     *
     * @param input de naamtekst
     * @return het leerlingnummer of een optellende waarde indien geen succes
     */
    public static String leerlingnummer(String input) {
        /*
        try {
            if (input.length() >= 15) { //TODO: Welke waarde nemen we?
                int haakje1_index = input.indexOf("(");
                
                //LEERLINGNUMMER uiteindelijk eruithalen
                return input.substring(haakje1_index + 1, input.indexOf(")"));
            }
        } catch (Exception e) {
            System.out.println("[Leerling/leerlingnummer(input)]: Niet-fatale fout: " + e.getMessage());
        }
        return "s" + ++teller;
         */
        return input;
    }
}
