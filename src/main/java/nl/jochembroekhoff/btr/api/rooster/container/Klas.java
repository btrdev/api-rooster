package nl.jochembroekhoff.btr.api.rooster.container;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

/**
 * Klas container.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class Klas {

    @NonNull
    @Getter
    private final String klasnaam;
    @Getter
    private final String niveau;

    /**
     * Parseer een klastekst. Voorbeeld: gv4c (4-vwo) &rarr; gv4c.
     *
     * @param input de klastekst
     * @return de klasnaam
     */
    public static String klasnaam(String input) {
        //return input.split(" ", 2)[0];
        return input.toLowerCase();
    }

    /**
     * Parseer een String naar een Klas. Voorbeeld: gv4c (vwo-4).
     *
     * @param input de klastekst
     * @return een vers Klas-object
     * @deprecated
     */
    public static Klas parseerOud(String input) {
        String[] input_split = input.split(" ", 2);
        String split_1 = "";
        if (input_split.length > 1) {
            split_1 = input_split[1];
        }
        return new Klas(input_split[0], split_1);
    }
}
