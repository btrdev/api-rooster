package nl.jochembroekhoff.btr.api.rooster.container;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Inner-element voor RoosterTekst. Dit is bijvoorbeeld 'fatlB' of 'g152', een
 * losstaand stukje tekst uit het rooster dus.
 *
 * @author Jochem Broekhoff
 */
@RequiredArgsConstructor
public class RoosterTekstDeco implements java.io.Serializable {

    @Getter
    private final String tekst;

    //Opmaak:
    @Getter
    @Setter
    private boolean doorgestreept = false;

    @Getter
    @Setter
    private boolean wijziging = false;

    @Getter
    @Setter
    private boolean tussentekst = false;

    @Override
    public String toString() {
        return tekst;
    }
}
