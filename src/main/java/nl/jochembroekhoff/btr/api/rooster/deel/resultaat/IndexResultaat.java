package nl.jochembroekhoff.btr.api.rooster.deel.resultaat;

import lombok.AllArgsConstructor;
import nl.jochembroekhoff.btr.api.IResultaat;
import nl.jochembroekhoff.btr.api.rooster.container.Indexering;

/**
 * IndexResultaat.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class IndexResultaat implements IResultaat {

    private final Indexering indexering;
    private boolean gelukt;

    @Override
    public Indexering getResultaat() {
        return indexering;
    }

    @Override
    public boolean gelukt() {
        return gelukt;
    }

    public void setGelukt(boolean gelukt) {
        this.gelukt = gelukt;
    }

}
