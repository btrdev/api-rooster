package nl.jochembroekhoff.btr.api.rooster.deel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nl.jochembroekhoff.btr.api.rooster.ApiRooster;
import nl.jochembroekhoff.btr.api.roostering.LesTijd;
import nl.jochembroekhoff.btr.api.rooster.container.roosterles.BasisLes;
import nl.jochembroekhoff.btr.api.rooster.container.RoosterTekst;
import nl.jochembroekhoff.btr.api.rooster.container.Week;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.LaadRoosterResultaat;
import nl.jochembroekhoff.btr.api.tools.JsoupUtil;
import org.joda.time.DateTime;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * LaadRooster Api onderdeel.
 *
 * @author Jochem Broekhoff
 */
public class LaadRooster {

    public static LaadRoosterResultaat laadRooster(String zoektekst, String type, Week week, String locatieSlug) {
        /*
        //TODO: Als het een lokaal is, moeten de eerste 5 letters ook werken.
        //Vindt uit of het een leerling, lokaal, docent of klas is
        String type = "s";
        if (zoektekst.length() == 5){
            type = "s"; //student
        }else if (zoektekst.matches("([A-Za-z]){3}")) {
            type = "t"; //teacher
            if (zoektekst.equals("kwt")) {
                type = "c"; //class, uitzondering 'kwt' klas voor sommige locaties
            }
        }else if (zoektekst.matches("([A-Za-z]){1}([0-9]){3}")) {
            type = "r"; //room
        }
         */

        try {
            //Zoek het gegeven op
            Integer index = null;

            zoektekst = zoektekst.toLowerCase();

            switch (type) {
                case "s":
                    index = ApiRooster.INDEXERINGEN.get(locatieSlug).getLeerlingen().get(zoektekst);
                    break;
                case "t":
                    index = ApiRooster.INDEXERINGEN.get(locatieSlug).getDocenten().get(zoektekst);
                    break;
                case "r":
                    index = ApiRooster.INDEXERINGEN.get(locatieSlug).getLokalen().get(zoektekst);
                    break;
                case "c":
                    index = ApiRooster.INDEXERINGEN.get(locatieSlug).getKlassen().get(zoektekst);
                    break;
            }

            //Stel de URL samen
            String indexPadded = String.format("%05d", index); //Maakt van 123 bijvoorbeeld 00123 (moet tot 5 karakters 'geprepadd' worden met nullen)
            String url = ApiRooster.getRoosterUrl() + locatieSlug + "/" + week.getIdentificator() + "/" + type + "/" + type + indexPadded + ".htm";

            System.out.println("Rooster laden van: " + url);

            Connection.Response resp = Jsoup.connect(url)
                    .userAgent(ApiRooster.getUA())
                    .timeout(ApiRooster.getTimeout())
                    .method(Connection.Method.GET)
                    .execute();
            Document doc = resp.parse();
            
            //Laatst bijgewerkt op...
            DateTime laatstBijgewerktOp = JsoupUtil.laatstBijgewerktOp(resp);

            //Ga het rooster parseren
            Element roostertabel = doc.select("body > center > table > tbody").first(); //Zoek de tabel op

            //Resultaatobjecten
            BasisLes[][] rooster2d = new BasisLes[7][10]; //TODO: Andere getallen?
            int roosterDagen = 1; //Aantal dagen dat dit rooster beschrijft, dagcount is logischerwijs niet hoger dan dagbreedte
            int roosterUren = 1; //Aantal lesuren per dag (meestal 8 of 9)
            LesTijd[] lestijden = new LesTijd[10];
            //TODO: Meta voor lestypes (excursie (s/t/c), clusterles (s/c), etc.)

            //Terugcount-map
            Map<String, Integer> terugmap = new HashMap<>(); //dagcount,lescount -> aant.lessen.terug

            //Counters
            int trcount = 0; //Teller: welke tr zijn we
            int lescount = 0; //Teller: welke les
            for (Element tr : roostertabel.select("> tr")) {
                if (trcount == 0) {
                    //Eerste rij met bijv. Maandag 15-05
                    roosterDagen = tr.select("> td").size() - 1; //Bereken het aantal dagen dat het rooster weergeeft
                } else if (trcount % 2 == 1) { //Als de teller oneven is
                    //Verwerk een rij lessen (horizontaal)
                    lescount++;
                    int dagcount = 0;
                    if (lescount > roosterUren) {
                        roosterUren = lescount; //Werk roosterUren bij indien het groter is dan de huidige waarde met lescount
                    }
                    for (Element td : tr.select("> td")) { //Loop over alle lessen (verticaal)
                        if (dagcount == 0) { //Rij 0: Geeft aan van hoe laat tot hoe laat het uur loopt
                            if (!type.equals("r")) {
                                //Lestijden werken alleen als het GEEN lokaal (room) is, bij een lokaal worden namelijk de lestijden niet gegeven
                                String kaalTijd = td.select("> table > tbody > tr > td").text().trim();
                                kaalTijd = kaalTijd.substring(kaalTijd.indexOf("(") + 1, kaalTijd.length() - 1);
                                //Parseer de lestijd en voeg deze simpelweg aan de List toe
                                lestijden[lescount] = LesTijd.parseer(kaalTijd);
                            }
                        } else { //Overige rijen: lessen
                            //Lestekst uitlezen
                            Element lesTbody = td.select("> table > tbody").first();
                            int spanVerticaal = Integer.parseInt(td.attr("rowspan")) / 2;
                            final List<RoosterTekst> lestekst = new ArrayList<>();

                            for (Element lesTr : lesTbody.select("> tr")) {
                                RoosterTekst rt = new RoosterTekst();
                                for (Element lesTd : lesTr.select("> td")) {
                                    rt.add(lesTd);
                                }
                                lestekst.add(rt);
                            }

                            //Houd er rekening mee dat er nog blokuren voor kunnen zijn
                            int terugcount = 0;
                            if (lescount > 1) {
                                for (int andereDag = 1; andereDag <= roosterDagen; andereDag++) {
                                    if (terugmap.containsKey((andereDag) + "," + lescount)) {
                                        terugcount++;
                                    } else {
                                        BasisLes anderePos = rooster2d[andereDag][lescount];
                                        if (anderePos == null) {
                                            break;
                                        }
                                    }
                                }
                            }

                            //Terugspan berekenen
                            if (spanVerticaal > 1) {
                                for (int spanOver = spanVerticaal - 1; spanOver >= 1; spanOver--) {
                                    int andereLes = lescount + spanOver;
                                    int nieuwDagcount = dagcount + terugcount;
                                    //System.out.println("terugmap put " + nieuwDagcount + "," + andereLes + "= " + spanOver);
                                    terugmap.put(nieuwDagcount + "," + andereLes, spanOver);
                                }
                            }

                            //Laat de les samenstellen door de Api
                            BasisLes rl = ApiRooster.stelLesSamen(lestekst, dagcount + terugcount, lescount, spanVerticaal);
                            rl.setBijbehorendeWeek(week);
                            rooster2d[dagcount + terugcount][lescount] = rl;
                        }
                        dagcount++;
                    }
                } //Else: overslaan, er staat toch niets in (lege tr)

                trcount++;
            }

            //Resultaat teruggeven
            return new LaadRoosterResultaat(rooster2d, roosterDagen, roosterUren, lestijden, week, laatstBijgewerktOp, true);
        } catch (Exception ex) {
            //todo
            ex.printStackTrace();
        }

        return new LaadRoosterResultaat(null, -1, -1, null, null, null, false);
    }
}
