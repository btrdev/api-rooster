package nl.jochembroekhoff.btr.api.rooster.container.roosterles;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import nl.jochembroekhoff.btr.api.roostering.LesTijd;
import nl.jochembroekhoff.btr.api.rooster.container.RoosterTekst;
import nl.jochembroekhoff.btr.api.rooster.container.Week;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.LaadRoosterResultaat;
import org.javatuples.Pair;
import org.joda.time.DateTime;

/**
 * Basis roosterles.
 *
 * @author Jochem Broekhoff
 */
@RequiredArgsConstructor
public class BasisLes implements java.io.Serializable {

    public static enum LesDoel {
        S, T, R, C
    }

    @Getter
    @Setter
    private String lesType = "BasisLes";
    @Getter
    @Setter
    private LesDoel lesDoel = LesDoel.S;

    //Overige metadata
    @Getter
    @Setter
    private Week bijbehorendeWeek;
    /**
     * Dag van de les. 0=zo, 6=za.
     */
    @Getter
    private final int dag;
    @Getter
    private final int lesUur;
    @Getter
    private final int lesSpan;

    //Tekst
    /**
     * Roostertekst. Beschrijving van dit lesuur dus.
     */
    @Getter
    protected final List<RoosterTekst> roosterTekst;

    public String getRoosterTekstVolledig() {
        String ret = "";
        for (RoosterTekst rt : roosterTekst) {
            ret += rt + " / ";
        }
        return ret.substring(0, ret.lastIndexOf(" / "));
    }

    //Metacheckers
    /**
     * Check of het een lege les is.
     *
     * @return of deze les leeg is (tussenuur, eerste vrij, etc.)
     */
    public boolean isLeeg() {
        boolean leeg = true;
        if (roosterTekst.size() > 0) {
            for (RoosterTekst rt : roosterTekst) {
                String rtstring = rt.toString();
                if (rtstring.length() > 0) {
                    leeg = false;
                }
                //Op een vage manier hebben sommige roosters een X op een leeg uur staan...
                //...zo wordt daar rekening mee gehouden
                if (rtstring.equals(("X"))) {
                    leeg = true;
                }
            }
        }
        return leeg;
    }

    /**
     * TODO check of uur een randuur is. Een randuur wil zeggen dat er of voor,
     * of na deze les geen andere bezette lessen meer zijn.
     *
     * @return
     */
    public boolean isRanduur() {
        return false;
    }

    /**
     * Check of de les een wijziging heeft. Als er minimaal één van de
     * roosterteksten rood is (dus een wijziging heeft) is de returnwaarde true.
     *
     * @return of de les een wijziging heeft
     */
    public boolean heeftWijziging() {
        if (roosterTekst.size() > 0) {
            for (RoosterTekst rt : roosterTekst) {
                if (rt.heeftWijziging()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check of de les doorgestreept is. Als er minimaal één van de
     * roosterteksten doorestreept is is de returnwaarde true.
     * <br>
     * Een andere veelgebruikte term voor doorgestreept is 'uitval'.
     *
     * @return of de les doorgestreept is
     */
    public boolean heeftDoorgestreept() {
        if (roosterTekst.size() > 0) {
            for (RoosterTekst rt : roosterTekst) {
                if (rt.heeftDoorgestreept()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Verkrijg de start- en eind-Date van deze les. Deze Date-objecten worden
     * gemaakt in de tijdzone <code>Europe/Amsterdam</code>. Het
     * LaadRoosterResultaat is vereist, het wordt gebruikt om het weeknummer te
     * verkrijgen.
     *
     * @deprecated Gebruik {@link LaadRoosterResultaat#getLestijden() } om de
     * lestijden op te halen. De index in die array is het nummer van het
     * lesuur.
     * @param lrr het LaadRoosterResultaat
     * @return een Pair met de start- en eind-Date objecten
     */
    public Pair<Date, Date> getVanTot(LaadRoosterResultaat lrr) {
        Date van, tot;

        //Haal de kale lestijden op
        LesTijd lestijdVan = lrr.getLestijden()[getLesUur()];
        LesTijd lestijdTot = lrr.getLestijden()[getLesUur()];
        if (getLesSpan() > 1) {
            lestijdTot = lrr.getLestijden()[getLesUur() + getLesSpan() - 1]; //-1 want standaardspan is 1
        }

        if (lestijdVan == null || lestijdTot == null) {
            return Pair.with(null, null);
        }

        //Bereken de date-objecten
        //TODO: Omvormen naar DateTime objecten ipv Date
        DateTime dtDezedag = lrr.getWeek().getDatum().plusDays(getDag());

        Calendar calVan = GregorianCalendar.getInstance(TimeZone.getTimeZone("Europe/Amsterdam"));
        calVan.set(Calendar.YEAR, dtDezedag.getYear());
        calVan.set(Calendar.WEEK_OF_YEAR, dtDezedag.getWeekOfWeekyear());
        calVan.set(Calendar.DAY_OF_WEEK, dtDezedag.getDayOfWeek());
        calVan.set(Calendar.HOUR_OF_DAY, lestijdVan.getStart().getUur());
        calVan.set(Calendar.MINUTE, lestijdVan.getStart().getMinuut());
        calVan.set(Calendar.SECOND, 0);
        van = calVan.getTime();

        Calendar calTot = GregorianCalendar.getInstance(TimeZone.getTimeZone("Europe/Amsterdam"));
        calTot.set(Calendar.YEAR, dtDezedag.getYear()); //TODO: Op een zekere manier het jaar verkrijgen
        calTot.set(Calendar.WEEK_OF_YEAR, dtDezedag.getWeekOfWeekyear());
        calTot.set(Calendar.DAY_OF_WEEK, dtDezedag.getDayOfWeek());
        calTot.set(Calendar.HOUR_OF_DAY, lestijdTot.getEinde().getUur());
        calTot.set(Calendar.MINUTE, lestijdTot.getEinde().getMinuut());
        calTot.set(Calendar.SECOND, 0);
        tot = calTot.getTime();

        return Pair.with(van, tot);
    }

    public String getTussentekst() {
        return "";
    }

}
