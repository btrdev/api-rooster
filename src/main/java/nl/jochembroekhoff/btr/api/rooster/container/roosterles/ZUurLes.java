package nl.jochembroekhoff.btr.api.rooster.container.roosterles;

import java.util.List;
import nl.jochembroekhoff.btr.api.rooster.container.RoosterTekst;

/**
 * Roosterles speciaal voor z-uur.
 *
 * @author Jochem Broekhoff
 */
public class ZUurLes extends BasisLes {

    public ZUurLes(int dag, int lesUur, int lesSpan, List<RoosterTekst> roosterTekst) {
        super(dag, lesUur, lesSpan, roosterTekst);
        super.setLesType(("ZUurLes"));
        super.setLesDoel(LesDoel.S);
    }

}
