package nl.jochembroekhoff.btr.api.rooster.container;

import lombok.Data;
import org.joda.time.DateTime;

/**
 *
 * @author Jochem Broekhoff
 */
@Data
public class Week implements java.io.Serializable {

    final DateTime datum;
    final String identificator;

    public int getWeeknummer() {
        return getDatum().getWeekOfWeekyear();
    }

    @Override
    public String toString() {
        return "Week[id=" + getIdentificator() + ", datum=" + datum.getYear() + "-" + datum.getMonthOfYear() + "-" + datum.getDayOfMonth() + "]";
    }
}
