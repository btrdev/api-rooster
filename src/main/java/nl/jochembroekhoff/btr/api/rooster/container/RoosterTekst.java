package nl.jochembroekhoff.btr.api.rooster.container;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Roostertekst. Elke regel met tekst in het rooster is een aparte RoosterTekst.
 * Een RoosterTekst bestaat uit meedere 'delen', dat zijn instanties van
 * RoosterTekstDeco. Zie RoosterTekstDeco voor meer info.
 *
 * @author Jochem Broekhoff
 */
public class RoosterTekst implements java.io.Serializable {

    public static enum WijzigingType {
        VAK,
        DOCENT,
        LOKAAL,
        ONZEKER
    }

    @Getter
    private final List<RoosterTekstDeco> velden = new LinkedList<>();

    public void add(Element html) {
        RoosterTekstDeco rtd = new RoosterTekstDeco(html.text());

        //Start checks voor wijziging en doorgestreept
        Elements innerFont = html.select("> font");
        if (innerFont.size() > 0) {
            Element font = innerFont.first();
            //Wijziging is er als de tekstkleur rood is (#FF0000)
            if (font.attr("color").equals("#FF0000")) {
                rtd.setWijziging(true);
            }
            //Lesuur is doorgestreept als het font element een strike element bevat
            if (font.select("> strike").size() > 0) {
                rtd.setDoorgestreept(true);
            }
            //Tussentekst als font het element een i element bevat
            if (font.select("> i").size() > 0) {
                rtd.setTussentekst(true);
            }
        }

        velden.add(rtd);
    }

    /**
     * Kijk of &eacute;&eacute;n of meer van de onderliggende RoosterTekstDeco
     * objecten een wijziging is. Als dit zo is, wordt er aangenomen dat het
     * geheel een wijziging is.
     *
     * @return
     */
    public boolean heeftWijziging() {
        for (RoosterTekstDeco rtd : velden) {
            if (rtd.isWijziging()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Kijk of &eacute;&eacute;n of meer van de onderliggende RoosterTekstDeco
     * objecten doorgestreept is. Als dit zo is, wordt er aangemomen dat het
     * geheel doorgestreept is.
     *
     * @return
     */
    public boolean heeftDoorgestreept() {
        for (RoosterTekstDeco rtd : velden) {
            if (rtd.isDoorgestreept()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Zoek uit welke vormen van wijziging er op deze RoosterTekst van
     * toepassing zijn.
     *
     * @return
     */
    public EnumSet<WijzigingType> welkeWijzigingen() {
        EnumSet<WijzigingType> typ = EnumSet.of(WijzigingType.ONZEKER);
        int i = 0;
        for (RoosterTekstDeco rtd : velden) {
            if (rtd.isWijziging()) {
                String txt = rtd.getTekst();
                boolean startVraagetekenOfPlus = txt.startsWith("?")
                        || txt.startsWith("+");
                boolean eindVraagtekenOfPlus = txt.endsWith("?")
                        || txt.endsWith("+");
                if (startVraagetekenOfPlus && eindVraagtekenOfPlus) {
                    typ.add(WijzigingType.VAK);
                } else if (startVraagetekenOfPlus && txt.endsWith("---")) {
                    typ.add(WijzigingType.DOCENT);
                } //TODO: Wijziging in LOKAAL
            }
        }
        return typ;
    }

    /**
     * Eenvoudige String-representatie van de RoosterTekst. Alle onderliggende
     * RoosterTekstDeco objecten worden aan elkaar geplakt met een spatie
     * ertussen. Hierbij gaat de opmaak natuurlijk verloren.
     * <br>
     * Let op: zgn tussentekst wordt niet meegenomen.
     *
     * @return
     */
    @Override
    public String toString() {
        String res = "";
        for (RoosterTekstDeco rtd : velden) {
            if (!rtd.isTussentekst()) {
                res += rtd + " ";
            }
        }
        return res.trim();
    }

    /**
     * Het tegenovergestelde van {@link #toString() }.
     *
     * @return alle tussenteksten gescheiden door spatie
     */
    public String getAlleTussentekst() {
        String res = "";
        for (RoosterTekstDeco rtd : velden) {
            if (rtd.isTussentekst()) {
                res += rtd + " ";
            }
        }
        return res.trim();
    }
}
