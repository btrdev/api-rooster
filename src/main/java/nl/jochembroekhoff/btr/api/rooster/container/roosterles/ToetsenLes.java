package nl.jochembroekhoff.btr.api.rooster.container.roosterles;

import java.util.List;
import nl.jochembroekhoff.btr.api.rooster.container.RoosterTekst;

/**
 * Roosterles speciaal voor toetsweek-toetsen.
 *
 * @author Jochem Broekhoff
 */
public class ToetsenLes extends BasisLes {

    public ToetsenLes(int dag, int lesUur, int lesSpan, List<RoosterTekst> roosterTekst) {
        super(dag, lesUur, lesSpan, roosterTekst);
        super.setLesType(("ToetsenLes"));
        super.setLesDoel(LesDoel.S);
    }

    /**
     * Verkijg het nummer van de bijbehorende toetsweek.
     *
     * @return het nummer van de toetsweek, meestal van 1 tm 4
     */
    public int getToetsweekNummer() {
        String twTekst = roosterTekst.get(0).getVelden().get(0).getTekst();
        return Integer.parseInt(twTekst.substring(twTekst.length() - 1, twTekst.length()));
    }

}
