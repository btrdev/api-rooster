package nl.jochembroekhoff.btr.api.rooster.container;

import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.joda.time.DateTime;

/**
 * Een indexering voor een locatie.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class Indexering implements java.io.Serializable {

    @NonNull
    @Getter
    List<Week> weken;
    @NonNull
    @Getter
    Map<String, Integer> klassen;
    @NonNull
    @Getter
    Map<String, Integer> docenten;
    @Getter
    Map<String, Integer> lokalen;
    @NonNull
    @Getter
    Map<String, Integer> leerlingen;
    @Getter
    DateTime laatstBijgewerktOp;

}
