package nl.jochembroekhoff.btr.api.rooster.deel;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nl.jochembroekhoff.btr.api.rooster.ApiRooster;
import nl.jochembroekhoff.btr.api.rooster.container.Docent;
import nl.jochembroekhoff.btr.api.rooster.container.Indexering;
import nl.jochembroekhoff.btr.api.rooster.container.Klas;
import nl.jochembroekhoff.btr.api.rooster.container.Leerling;
import nl.jochembroekhoff.btr.api.rooster.container.Lokaal;
import nl.jochembroekhoff.btr.api.rooster.container.Week;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.IndexResultaat;
import nl.jochembroekhoff.btr.api.tools.JsoupUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Api-onderdeel voor indexering.
 *
 * @author Jochem Broekhoff
 */
public class Index {

    public static JsonParser JSON_PARSER = new JsonParser();

    /**
     * Voer een index-actie uit met de gegeven slug van een locatie.
     *
     * @param locatieSlug iets als "Goes"
     * @return het IndexResultaat met de resultaten
     */
    public static IndexResultaat indexeer(String locatieSlug) {

        DateTime laatstBijgewerktOp = null;
        final List<Week> weken = new ArrayList<>();
        final Map<String, Integer> klassen = new HashMap<>();
        final Map<String, Integer> docenten = new HashMap<>();
        final Map<String, Integer> lokalen = new HashMap<>();
        final Map<String, Integer> leerlingen = new HashMap<>();

        try {
            Connection.Response resp = Jsoup.connect(ApiRooster.getRoosterUrl() + "/" + locatieSlug + "/frames/navbar.htm")
                    .method(Connection.Method.GET)
                    .userAgent(ApiRooster.getUA())
                    .timeout(ApiRooster.getTimeout())
                    .execute();

            Document doc = resp.parse();

            ////////////////////////////////////////////////////////////////////
            // Laatst bijgewerkt op ...
            laatstBijgewerktOp = JsoupUtil.laatstBijgewerktOp(resp);

            ////////////////////////////////////////////////////////////////////
            // Weken
            //TODO: Met formatter/parser
            Elements weekOptions = doc.select("[name=\"week\"] option");
            for (Element option : weekOptions) {
                String weekidentif = option.val();
                String datumdef = option.text().trim();
                if (weekidentif.length() > 0 && datumdef.length() > 0) {
                    String[] splits = datumdef.split("-");
                    if (splits.length != 3) {
                        continue;
                    }
                    DateTime datum = new DateTime(Integer.parseInt(splits[2]), Integer.parseInt(splits[1]), Integer.parseInt(splits[0]), 0, 0, DateTimeZone.forID("Europe/Amsterdam"));
                    weken.add(new Week(datum, weekidentif));
                }
            }

            ////////////////////////////////////////////////////////////////////
            // Klassen, Docenten, Lokalen en Leerlingen
            //Haal de javascript uit navbar.htm
            String headscript = doc.select("head > script").get(1).html();

            int i = 0;
            for (String l : headscript.split("\\r?\\n")) {
                int j = 1;
                //System.out.println(i + ": " + l);
                int blokhaak1 = l.indexOf("[");
                if (blokhaak1 != -1) {
                    try {
                        String lt = l.trim();

                        String arrString = l.substring(blokhaak1, l.length() - 1);
                        if (lt.startsWith("var classes")) {
                            JsonArray arrJson = (JsonArray) JSON_PARSER.parse(arrString);
                            verwerkKlassen(klassen, arrJson, j);
                        } else if (lt.startsWith("var teachers")) {
                            JsonArray arrJson = (JsonArray) JSON_PARSER.parse(arrString);
                            verwerkDocenten(docenten, arrJson, j);
                        } else if (lt.startsWith("var rooms")) {
                            JsonArray arrJson = (JsonArray) JSON_PARSER.parse(arrString);
                            verwerkLokalen(lokalen, arrJson, j);
                        } else if (lt.startsWith("var students")) {
                            JsonArray arrJson = (JsonArray) JSON_PARSER.parse(arrString);
                            verwerkLeerlingen(leerlingen, arrJson, j);
                        }
                    } catch (Exception e) {
                        System.out.println("WARN: " + e.getMessage());
                    }
                }
            }

            Indexering index = new Indexering(weken, klassen, docenten, lokalen, leerlingen, laatstBijgewerktOp);
            IndexResultaat ir = new IndexResultaat(index, true);
            return ir;
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("[Indexeer]: Foutmelding: " + ex.getMessage());
            //Niet nodig, als de lengte van 'namenlijst' kleiner is dan 1, wordt
            //het resultaat automatisch op niet gelukt ingesteld.
            return new IndexResultaat(null, false);
        }
    }

    private static void verwerkKlassen(Map<String, Integer> klassen, JsonArray arrJson, int j) {
        for (JsonElement arrElem : arrJson) {
            klassen.put(Klas.klasnaam(arrElem.getAsString()), j);
            j++;
        }
    }

    private static void verwerkDocenten(Map<String, Integer> docenten, JsonArray arrJson, int j) {
        for (JsonElement arrElem : arrJson) {
            docenten.put(Docent.docentcode(arrElem.getAsString()), j);
            j++;
        }
    }
    
    private static void verwerkLokalen(Map<String, Integer> lokalen, JsonArray arrJson, int j) {
        for (JsonElement arrElem : arrJson) {
            lokalen.put(Lokaal.lokaalcode(arrElem.getAsString()), j);
            j++;
        }
    }

    private static void verwerkLeerlingen(Map<String, Integer> leerlingen, JsonArray arrJson, int j) {
        for (JsonElement arrElem : arrJson) {
            leerlingen.put(Leerling.leerlingnummer(arrElem.getAsString()), j);
            j++;
        }
    }
}
