package nl.jochembroekhoff.btr.api.rooster;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.IBtrApi;
import nl.jochembroekhoff.btr.api.rooster.checks.GlobaleChecks;
import nl.jochembroekhoff.btr.api.rooster.container.Indexering;
import nl.jochembroekhoff.btr.api.rooster.container.RoosterTekst;
import nl.jochembroekhoff.btr.api.rooster.container.Week;
import nl.jochembroekhoff.btr.api.rooster.container.roosterles.*;
import nl.jochembroekhoff.btr.api.rooster.deel.Index;
import nl.jochembroekhoff.btr.api.rooster.deel.LaadRooster;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.IndexResultaat;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.LaadRoosterResultaat;
import org.apache.commons.lang3.ArrayUtils;
import org.javatuples.Pair;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Rooster API basisklasse, voor instellingen, indexering etc.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class ApiRooster implements IBtrApi {

    //Instellingen
    private static String ROOSTER_URL = "http://rooster.calvijncollege.nl/";
    private static int TIMEOUT = 2000;
    public static Map<String, String> LOCATIES = new HashMap<>();
    private static final String VERSIE = "1.0.0";

    //Indexering-cache
    public static final HashMap<String, Indexering> INDEXERINGEN = new HashMap<>();

    //Tools
    public static final String[] MAANDEN_MAP = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] DAGEN_MAP = new String[]{"Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag"};

    /**
     * Indexeer de locaties.
     *
     * @return of de indexeringsactie geslaagd is
     */
    public static boolean indexeer() {
        if (LOCATIES.size() > 0) {
            for (String locatieSlug : LOCATIES.keySet()) {
                IndexResultaat ir = Index.indexeer(locatieSlug);
                if (ir.gelukt()) {
                    Indexering index = ir.getResultaat();
                    INDEXERINGEN.put(locatieSlug, index);
                } else {
                    //TODO: Foutafhandeling
                    return false;
                }
            }
        } else {
            System.out.println("[ApiRooster/indexeer()]: FATAAL: Geen locaties opgegeven!");
            return false;
        }
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////
    //INSTELLINGEN:
    public static void setRoosterUrl(String url) {
        ApiRooster.ROOSTER_URL = url;
    }

    public static String getRoosterUrl() {
        return ApiRooster.ROOSTER_URL;
    }

    public static void setTimeout(int millis) {
        ApiRooster.TIMEOUT = millis;
    }

    public static int getTimeout() {
        return ApiRooster.TIMEOUT;
    }

    /**
     * UserAgent.
     *
     * @return de UA string
     */
    public static String getUA() {
        return "BtrApiRooster/" + ApiRooster.getVersie();
    }

    public static String getVersie() {
        return ApiRooster.VERSIE;
    }

    ////////////////////////////////////////////////////////////////////////////
    //TOOLS:
    public static int getMaandIndex(String in) {
        return ArrayUtils.indexOf(MAANDEN_MAP, in);
    }

    public static BasisLes stelLesSamen(List<RoosterTekst> roosterTekst, int dag, int lesUur, int lesSpan) {
        /////START MET AL DE CHECKS
        if (GlobaleChecks.Leerling.isToetsweek(roosterTekst)) {
            return new ToetsenLes(dag, lesUur, lesSpan, roosterTekst);
        } else if (GlobaleChecks.Leerling.isZUur(roosterTekst)) {
            return new ZUurLes(dag, lesUur, lesSpan, roosterTekst);
        } else if (GlobaleChecks.Docent.isCaoVo(roosterTekst)) {
            return new CaoVo(dag, lesUur, lesSpan, roosterTekst);
        } else {
            //Als er geen goede optie gevonden is... een kale les
            return new BasisLes(dag, lesUur, lesSpan, roosterTekst);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //MAIN:
    public static void main(String... args) {
        //Initialiseer de Api door de Rooster URL in te stellen en de locaties toe tevoegen
        //ApiRooster.setRoosterUrl("http://jochembroekhoff.nl/rooster/lesrooster");
        ApiRooster.setRoosterUrl("http://rooster.calvijncollege.nl/");
        ApiRooster.LOCATIES.put("Goes", "Goes Klein Frankrijk");
        ApiRooster.LOCATIES.put("GoesNoordhoeklaan", "Goes Noordhoeklaan");
        ApiRooster.LOCATIES.put("GoesStationspark", "Goes Stationspark");
        ApiRooster.LOCATIES.put("KrabbendijkeAppelstraat", "Krabbendijke Appelstraat");
        ApiRooster.LOCATIES.put("KrabbendijkeKerkpolder", "Krabbendijke Kerkpolder");
        ApiRooster.LOCATIES.put("Middelburg", "Middelburg");
        ApiRooster.LOCATIES.put("Tholen", "Tholen");

        //Timing
        System.out.println("Indexeren...\n\n[TIMING START]: " + new Timestamp(System.currentTimeMillis()));
        if (!ApiRooster.indexeer()) { //Voer de indexeer-actie uit
            System.out.println("INDEXEREN GEFAALD");
        }
        System.out.println("[TIMING   END]: " + new Timestamp(System.currentTimeMillis()));
        /*
        //Voorbeeld: Dit print de gehele index uit
        for (Map.Entry<String, Indexering> entry : INDEXERINGEN.entrySet()) {
            Indexering index = entry.getValue();

            System.out.println("\n\n----------------------------------------------------");
            System.out.println("-- INDEX VOOR: " + LOCATIES.get(entry.getKey()));
            System.out.println("-- BIJGEWERKT OP: " + index.getLaatstBijgewerktOp());
            System.out.println("----------------------------------------------------\n");

            System.out.println(">>>>>>> LEERLINGEN <<<<<<<");
            for (Map.Entry<String, Integer> ll : index.getLeerlingen().entrySet()) {
                System.out.println("[#" + ll.getValue() + "]: " + ll.getKey());
            }
            System.out.println();

            System.out.println(">>>>>>> DOCENTEN <<<<<<<");
            for (Map.Entry<String, Integer> docent : index.getDocenten().entrySet()) {
                System.out.println("[#" + docent.getValue() + "]: " + docent.getKey());
            }
            System.out.println();
        }
         */
 /*
        System.out.println("\n------------------------\n\nVul gegevens in:");
        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.print(">> Code: ");
        String code = reader.next();
        System.out.print(">> Type[s/t/r/c]: ");
        String type = reader.next();
        System.out.print(">> Week: ");
        int week = reader.nextInt();
        System.out.print(">> Locatie: ");
        String locatie = reader.next();

        System.out.print(">> Json-export: ");
        String jsonUit = reader.next();
        System.out.print(">> Json-export-index: ");
        String jsonUitIndex = reader.next();
        System.out.print(">> Serialization-export: ");
        String serUit = reader.next();
        System.out.print(">> Serialization-export-index: ");
        String serUitIndex = reader.next();
         */
        String code = "g035";
        String type = "r";
        Week week = new Week(new DateTime(2018, 5, 2, 0, 0, DateTimeZone.forID("Europe/Amsterdam")), "06");
        new File("/tmp/btrooster").mkdirs();
        String locatie = "Goes";
        String jsonUit = "/tmp/btrooster/r.json";
        String jsonUitIndex = "/tmp/btrooster/i.json";
        String serUit = "/tmp/btrooster/r.ser";
        String serUitIndex = "/tmp/btrooster/i.ser";

        System.out.println("------------------------\n\n");

        LaadRoosterResultaat lrr = LaadRooster.laadRooster(code, type, week, locatie);
        //LaadRoosterResultaat lrr = LaadRooster.laadRooster("14954", "s", 27, "Goes");

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(lrr);
        String json_index = gson.toJson(ApiRooster.INDEXERINGEN);

        //Schrijf naar bestand
        try (PrintWriter out = new PrintWriter(jsonUit)) {
            out.println(json);
        } catch (Exception e) {
        }
        try (PrintWriter out = new PrintWriter(jsonUitIndex)) {
            out.println(json_index);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (PrintWriter pw = new PrintWriter(serUit)) {
            pw.print(BtrApi.serializableToString(lrr, true));
        } catch (IOException i) {
            i.printStackTrace();
        }
        try (PrintWriter pw = new PrintWriter(serUit + "_nc")) {
            pw.print(BtrApi.serializableToString(lrr, false));
        } catch (IOException i) {
            i.printStackTrace();
        }
        try (PrintWriter pw = new PrintWriter(serUitIndex)) {
            pw.print(BtrApi.serializableToString(ApiRooster.INDEXERINGEN, true));
        } catch (IOException i) {
            i.printStackTrace();
        }
        try (PrintWriter pw = new PrintWriter(serUitIndex + "_nc")) {
            pw.print(BtrApi.serializableToString(ApiRooster.INDEXERINGEN, false));
        } catch (IOException i) {
            i.printStackTrace();
        }

        //Weer terug uitlezen
        //Dit uncommenten als je de JSON wilt testen:
        //LaadRoosterResultaat lrr_geladen = gson.fromJson(json, LaadRoosterResultaat.class);
        LaadRoosterResultaat lrr_geladen = null;
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(serUit));
            String str = new String(bytes, Charset.defaultCharset());
            lrr_geladen = BtrApi.stringToSerializable(str, LaadRoosterResultaat.class, true);
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("LaadRoosterRestulaat class not found");
            c.printStackTrace();
        }

        if (lrr_geladen.gelukt()) {

            BasisLes[][] lessen = lrr_geladen.getRooster2d();
            int iDag = 0;
            int iUur = 0;
            for (BasisLes[] dag : lessen) {
                System.out.println("---dag#" + iDag + ": " + DAGEN_MAP[iDag]);
                if (dag != null) {
                    iUur = 0;
                    for (BasisLes uur : dag) {
                        System.out.println("   ---uur" + iUur);
                        if (uur != null) {
                            Pair<Date, Date> vt = uur.getVanTot(lrr);
                            if (vt == null || vt.getValue0() == null || vt.getValue1() == null) {
                                System.out.println("      > (Lestijd onbekend)");
                            } else {
                                System.out.println("      > " + vt.getValue0() + " tot " + vt.getValue1());
                            }

                            String extra = "";
                            if (uur.getRoosterTekst().size() > 0) {
                                RoosterTekst ert = uur.getRoosterTekst().get(0);
                                if (ert.heeftDoorgestreept()) {
                                    extra += ", DOORGESTREEPT";
                                }
                                if (ert.heeftWijziging()) {
                                    extra += ", WIJZIGING";
                                }
                            }
                            if (uur.isLeeg()) {
                                extra += ", LEEG";
                            }

                            System.out.printf("      > [Dag %d, Les %d]: %s {Span=%d%s, Type=%s}\n",
                                    uur.getDag(),
                                    uur.getLesUur(),
                                    uur.getRoosterTekstVolledig(),
                                    uur.getLesSpan(),
                                    extra,
                                    uur.getLesType());
                        }
                        iUur++;
                    }
                }
                iDag++;
            }
            System.out.println("------------------------");
            System.out.printf("Aantal dagen: %d\n", lrr_geladen.getDagen());
            System.out.printf("Aantal uren per dag: %d\n", lrr_geladen.getUren());
        }

    }
}
