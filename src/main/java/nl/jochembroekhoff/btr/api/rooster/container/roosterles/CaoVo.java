package nl.jochembroekhoff.btr.api.rooster.container.roosterles;

import java.util.List;
import nl.jochembroekhoff.btr.api.rooster.container.RoosterTekst;

/**
 * Roosterles speciaal voor cao-vo. Alleen van toepassing op docenten (t,
 * teacher).
 *
 * @author Jochem Broekhoff
 */
public class CaoVo extends BasisLes {

    public CaoVo(int dag, int lesUur, int lesSpan, List<RoosterTekst> roosterTekst) {
        super(dag, lesUur, lesSpan, roosterTekst);
        super.setLesType(("CaoVo"));
        super.setLesDoel(LesDoel.T);
    }

}
