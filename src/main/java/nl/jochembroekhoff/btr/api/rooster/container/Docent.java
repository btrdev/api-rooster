package nl.jochembroekhoff.btr.api.rooster.container;

import lombok.AllArgsConstructor;
import lombok.Getter;

import nl.jochembroekhoff.btr.api.roostering.Naam;

/**
 * Docent container met parseerder.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class Docent {

    @Getter
    private final Naam naam;

    /**
     * Verkrijg een drieletterige docentencode uit een stuk tekst voor een
     * docent. Dit is simpelweg het stuk tekst naar kleine letters.
     * 
     * Oude methode was bijvoorbeeld: Selie, P.T.R. (slp) geeft slp.
     *
     * @param input docent-tekst
     * @return drieletterige docentencode
     */
    public static String docentcode(String input) {
        //return input.substring(input.indexOf("(") + 1, input.length() - 1);
        return input.toLowerCase();
    }
}
