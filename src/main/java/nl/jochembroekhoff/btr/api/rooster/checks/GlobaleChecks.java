package nl.jochembroekhoff.btr.api.rooster.checks;

import java.util.List;
import nl.jochembroekhoff.btr.api.rooster.container.RoosterTekst;
import nl.jochembroekhoff.btr.api.rooster.container.RoosterTekstDeco;

/**
 * Checs voor toetsweek.
 *
 * @author Jochem Broekhoff
 */
public class GlobaleChecks {

    public static class Leerling {

        public static boolean isToetsweek(List<RoosterTekst> rt) {
            for (RoosterTekst tekst : rt) {
                String txt = tekst.toString();
                return (txt.length() == 3 && txt.startsWith("TW"));
            }
            return false;
        }

        public static boolean isZUur(List<RoosterTekst> rt) {
            //TODO: (misschien) Robuustere check, dit is nogal statisch
            if (rt.size() == 2) {
                return (rt.get(0).toString().equals("z-uur .")
                        && rt.get(1).toString().equals("zst"));
            } else if (rt.size() > 2) {
                boolean heeftZStreepjUur = false;
                boolean heeftZST = false;
                boolean heeftZ = false;
                for (RoosterTekst rtDeel : rt) {
                    for (RoosterTekstDeco rtDeelDeco : rtDeel.getVelden()) {
                        String txt = rtDeelDeco.getTekst().trim();
                        if (txt.equals("z-uur")) {
                            heeftZStreepjUur = true;
                        } else if (txt.equals("zst")) {
                            heeftZST = true;
                        } else if (txt.equals("z")) {
                            heeftZ = true;
                        }
                    }
                }
                return heeftZStreepjUur && heeftZST && heeftZ;
            }
            return false;
        }
    }

    public static class Docent {

        public static boolean isCaoVo(List<RoosterTekst> rt) {
            if (rt.size() > 0) {
                return rt.get(0).toString().equals("cao-vo");
            }
            return false;
        }
    }
}
