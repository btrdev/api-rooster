package nl.jochembroekhoff.btr.api.rooster.container;

/**
 * Lokaal container.
 *
 * @author Jochem Broekhoff
 */
public class Lokaal {

    /**
     * Verkrijg de lokaalcode uit een stuk lokaalcode.
     *
     * @param input lokaalcode
     * @return verwerkte lokaalcode
     */
    public static String lokaalcode(String input) {
        if (input.length() > 5) {
            return input.substring(0, 5);
        }
        return input;
    }
}
