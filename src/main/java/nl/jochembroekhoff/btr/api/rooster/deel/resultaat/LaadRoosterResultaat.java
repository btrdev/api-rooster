package nl.jochembroekhoff.btr.api.rooster.deel.resultaat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.jochembroekhoff.btr.api.IResultaat;
import nl.jochembroekhoff.btr.api.rooster.container.Week;
import nl.jochembroekhoff.btr.api.roostering.LesTijd;
import nl.jochembroekhoff.btr.api.rooster.container.roosterles.BasisLes;
import org.javatuples.Quartet;
import org.joda.time.DateTime;

/**
 * LaadRoosterResultaat.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class LaadRoosterResultaat implements IResultaat {

    @Getter
    private final BasisLes[][] rooster2d;
    @Getter
    private final int dagen;
    @Getter
    private final int uren;
    /**
     * Array met lestijden. De index in de array duidt het nummer van de les
     * aan. Zo is dan ook voor les nr. 1 de lestijd:
     * <code>getLestijden()[1]</code>.
     */
    @Getter
    private final LesTijd[] lestijden;
    @Getter
    private final Week week;
    @Getter
    private final DateTime laatstBijgewerktOp;

    private boolean gelukt;

    /**
     * Verkrijg een geheel van rooster in de vorm van ee Quartet. De velden zijn
     * allemaal individueel te benaderen door de bijbehorende getters. De
     * Quartet bestaat uit <code>BasisLes[][] rooster2d</code>,
     * <code>int dagen</code>, <code>int uren</code> en
     * <code>List&lt;LesTijd&gt; lestijden</code>.
     *
     * @return de Quartet met informatie
     */
    @Override
    public Quartet<BasisLes[][], Integer, Integer, LesTijd[]> getResultaat() {
        return Quartet.with(rooster2d, dagen, uren, lestijden);
    }

    @Override
    public boolean gelukt() {
        return gelukt;
    }

    public void setGelukt(boolean gelukt) {
        this.gelukt = gelukt;
    }

}
